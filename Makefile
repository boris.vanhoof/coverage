all: main

main:
	make -C src all

coverage:
	make -C src coverage

clean:
	make -C src clean
