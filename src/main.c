#include <stdio.h>
#include <stdlib.h>

#include "bar.h"
#include "foo.h"

int main(int argc, char ** argv)
{
    int x;
    printf("hello world!\n");
    x = bar(3, foo());
    printf("x = %d\n", x);
    exit(0);
}
