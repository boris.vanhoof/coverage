#include <stdio.h>
#include "bar.h"

static int baz()
{
    return 10;
}

int bar(int a, int b)
{
    if (a > 10)
    {
        return 10;
    }
    else
    {
        return a * b + baz();
    }
}

int bar2()
{
    return 5000;
}
